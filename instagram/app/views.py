from django.shortcuts import render
from django.views.generic import View, TemplateView, DetailView, FormView, ListView
from app.models import Post, InstagramUser
from app.forms import InstagramUserForm
import sys

# TODO Change the content displayed based on if the user is logged in or not
class IndexView(ListView):
    template_name = "app/index.html"
    model = Post

class RegisterView(FormView):
    template_name = 'app/register.html'
    form_class = InstagramUserForm
    success_url = '/'

    def form_valid(self, form):
        # TODO Handle profile picture by uploading it to CDN and not local file system
        user = InstagramUser.objects.create_user(
            email=form.cleaned_data['email'],
            username=form.cleaned_data['username'],
            name=form.cleaned_data['name'],
            profile_picture=form.cleaned_data['profile_picture'],
            password=form.cleaned_data['password'],
        )
        return super(RegisterView, self).form_valid(form)

class CreatePostView(TemplateView):
    template_name = "app/create_post.html"

class PostView(DetailView):
    model = Post

class LoginView(TemplateView):
    template_name = "app/login.html"

class LogoutView(View):
    pass

class AccountEditView(FormView):
    template_name = 'app/edit_account.html'
    form_class = InstagramUserForm
    success_url = '/'

    def form_valid(self, form):
        user = InstagramUser.objects.create_user(
            email=form.cleaned_data['email'],
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password'],
        )
        return super(RegisterView, self).form_valid(form)

