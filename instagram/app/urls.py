from django.conf import settings
from django.conf.urls import patterns, url, include
from app.views import IndexView, RegisterView, CreatePostView, LoginView, LogoutView, AccountEditView

urlpatterns = patterns('',
    (r'^/?$', IndexView.as_view()),
    (r'^register/?$', RegisterView.as_view()),
    (r'^login/?$', LoginView.as_view()),
    (r'^logout/?$', LogoutView.as_view()),
    (r'^posts/create/?$', CreatePostView.as_view()),
    (r'^accounts/edit/?$', AccountEditView.as_view()),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT
        }),
    )


